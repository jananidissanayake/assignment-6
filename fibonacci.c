#include<stdio.h>
int fibonacciSeq(int n);
 
 
int main(void)
{    
    int Seqno;
    
    printf("Enter the sequence no: ");
    scanf("%d", &Seqno);       
    
    for(int n = 0; n <=Seqno; n++)
    {
        printf("%d ", fibonacciSeq(n));
        printf("\n");
    }
    
    return 0; 
}
 
 
int fibonacciSeq(int n)
{    
    
    //base condition
    if(n == 0 || n == 1)
    {
        return n;
    }
    
    else
    {
        // recursive call
        return fibonacciSeq(n-1) + fibonacciSeq(n-2);
    }
    
}
