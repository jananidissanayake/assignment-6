#include<stdio.h>
void pattern(int n);
int main()
{
    int no;
    
    printf("Enter the pattern number:");
    scanf("%d",&no);
    pattern(no);

    return 0;
}

void pattern(int n){
    
    if(n != 0){
        pattern(n-1);
        for(int i=n;i>=1;i--)
            printf("%d",i);
        printf("\n");
    }
}
